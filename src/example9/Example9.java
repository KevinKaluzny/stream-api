package example9;

import java.util.stream.IntStream;

public class Example9 {
    public static void main(String[] args) {
        IntStream.range(1, 6)
                .forEach(System.out::println);

        // 1
        // 2
        // 3
        // 4
        // 5
    }
}
