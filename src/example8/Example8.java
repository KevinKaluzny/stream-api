package example8;

import java.util.stream.IntStream;

public class Example8 {
    public static void main(String[] args) {
        IntStream.of(1, 2, 3, 4, 5, 6)
                .forEach(System.out::println);

        // 1
        // 2
        // 3
        // 4
        // 5
        // 6
    }
}
