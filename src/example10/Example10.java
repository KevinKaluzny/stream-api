package example10;

import java.util.function.Predicate;
import java.util.stream.Stream;

public class Example10 {
    public static void main(String[] args) {
        Stream<String> namesStream =
                Stream.of("John", "Marry", "George", "Paul", "Alice", "Ann");

        Predicate<String> hasName = name -> name.equals("Alice");

        namesStream.anyMatch(hasName);  // ok
        namesStream.noneMatch(hasName); // exception

        // Exception in thread "main" java.lang.IllegalStateException: stream has already been operated upon or closed
        // at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:229)
        // at java.util.stream.ReferencePipeline.noneMatch(ReferencePipeline.java:459)
        // at bosch.com.article.StreamApiDemo.fourthDemo(StreamDemo.java:88)
        // at bosch.com.article.StreamApiDemo.main(StreamDemo.java:488)
    }
}
