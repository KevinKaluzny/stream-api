package example11;

import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Example11 {
    public static void main(String[] args) {
        Supplier<Stream<String>> namesStreamSupplier =
                () -> Stream.of("John", "Marry", "George", "Paul", "Alice", "Ann");

            Predicate<String> hasName = name -> name.equals("Alice");

            namesStreamSupplier.get().anyMatch(hasName);  // ok
            namesStreamSupplier.get().noneMatch(hasName); // ok
    }
}
