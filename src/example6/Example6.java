package example6;

import java.util.Arrays;
import java.util.List;

public class Example6 {
    public static void main(String[] args) {
        List<String> namesList = Arrays.asList("John", "Marry", "George", "Paul", "Alice", "Ann");

        namesList
                .stream()
                .filter(e -> {
                    System.out.println("filter: " + e);
                    return true;
                })
                .forEach(e -> System.out.println("forEach: " + e));
    }
}
