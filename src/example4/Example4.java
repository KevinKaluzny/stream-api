package example4;

import java.util.Arrays;
import java.util.List;

public class Example4 {
    public static void main(String[] args) {
        List<String> namesList = Arrays.asList("John", "Marry", "George", "Paul", "Alice", "Ann");

        namesList
                .stream()
                .filter(e -> {
                    System.out.println("filter: " + e);
                    return true;
                });
    }
}
