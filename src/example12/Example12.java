package example12;

import java.util.Arrays;
import java.util.List;

public class Example12 {
    public static void main(String[] args) {
        List<Employee> employees =
                Arrays.asList(
                        new Employee(1, 2000d, "Risk Department", Employee.DayJob.FULL_TIME),
                        new Employee(2,2500d, "Scoring Department", Employee.DayJob.FULL_TIME),
                        new Employee(3,2600d, "Scoring Department", Employee.DayJob.FULL_TIME),
                        new Employee(4,2700d, "Credit Department", Employee.DayJob.FULL_TIME),
                        new Employee(5,2700d, "Credit Department", Employee.DayJob.PART_TIME)
                );

        employees
                .stream()
                .map(employee -> employee.getDivision())
                .forEach(System.out::println);

        // Risk Department
        // Scoring Department
        // Scoring Department
        // Credit Department
        // Credit Department

        employees
                .stream()
                .filter(employee -> employee.getSalary() > 2600d)
                .forEach(employee -> System.out.println(employee.toString()));

        // Employee {
        //      id: 4,
        //      salary: 2700.0,
        //      division: Credit Department,
        //      dayJob: FULL_TIME}
        // Employee {
        //     id: 5,
        //     salary: 2700.0,
        //     division: Credit Department,
        //     dayJob: PART_TIME}
    }
}
